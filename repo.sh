function update_repo_locally () {
    if [ ! -z $1 ] && (ls -d $1 >& /dev/null) ; then REPO_DIR_GIT=$1 ;
    else
	echo "I could not find local repo $1"
	return 1
    fi
    echo 'Updating git repo' $REPO_DIR_GIT
    if cd $REPO_DIR_GIT && git status >& /dev/null ; then
        echo "Repo exists. git-pulling to origin/master : git pull origin/master"
        git fetch origin master
	git pull
	if ls post-install.sh ; then
	    echo "Running post-install script post-install.sh"
	    source post-install.sh
	    return 0
	else
            echo "Running setup script start.sh"
            source start.sh
	    return 0
	fi
    else
        echo "Argument $1 is not a local git repo"
        return 1
    fi
}
function install_repo () {
    if [ -z $1 ] || [ -z $2 ] ; then
	echo ' install_repo: Wrong arguments!!! Usage:'
	echo ' \% install_repo https://gitlab.cern.ch/sevdokim/remote-repo /path/to/local/repo ' ;
	return 1 ;
    fi
    NEW_REPO_NAME=$1
    REPO_DIR_GIT=$2
    (echo ${USER}'@'$(uname -n) at $(date) ' : New installation!')
    (echo ${USER}'@'$(uname -n) at $(date) ' : New installation!') > /tmp/install.log
    if [ -d $REPO_DIR_GIT ] && cd $REPO_DIR_GIT; then
	if git status >& /dev/null ; then
	    echo "Some local repo already exists in $REPO_DIR_GIT "
	    echo "Please check. Stopping..."
	    return 1
	fi
	cd -
    fi
    if [ -e $REPO_DIR_GIT ] ; then
        mv $REPO_DIR_GIT ${REPO_DIR_GIT}.bkp
        echo "Found $REPO_DIR_GIT but no git repo initialized. Renamed it to ${REPO_DIR_GIT}.bkp"
        echo "Found $REPO_DIR_GIT but no git repo initialized. Renamed it to ${REPO_DIR_GIT}.bkp" >> /tmp/install.log
    fi
    echo "Clonning $NEW_REPO_NAME to $REPO_DIR_GIT"
    if git clone $NEW_REPO_NAME $REPO_DIR_GIT >> install.log && cd $REPO_DIR_GIT ; then
        echo "Clonned successfully. See $(pwd)/install.log Starting environment..."
        cat /tmp/install.log >> install.log
	if ls post-install.sh ; then
	    echo "Running post-install script post-install.sh"
	    source post-install.sh 
	else
	    source start.sh 
	fi
        echo "$(date)' : Started local $REPO_DIR_GIT at ' ${USER}'@'$(uname -n)" >> install.log
	return 0
    else
	echo "Problem with installing repo!!! Stopping...">>install.log
	return 1
    fi
}
export -f install_repo
function update_repo_remotely () {
    if cd $1 && git status >& /dev/null ; then
	echo "updating remote repo" $(cat $1/repo-name)
	git fetch origin master
	git status >& /dev/null
        #git status -u --column -s | awk -F' ' '{ print $2 }' > /tmp/commit
        #for file in $(git status -u --column -s | awk -F' ' '{ print $2 }')
        #do git add $file ; done
	git add -A
	if [ -z "$COMMIT_MESSAGE" ] ; then COMMIT_MESSAGE=$(git status -s) ; fi
        git commit -a -m "$COMMIT_MESSAGE"
	git pull --rebase
        git push
	if ls post-install.sh ; then
	    echo "Running post-install script post-install.sh"
	    source post-install.sh 
	else
	    source start.sh ;
	fi
	return 0
    else
        echo "No repo $1 found."
        echo "Try to install it first!"
        return 1
    fi
}
export -f update_repo_remotely
function if_repo_changed () {
    updated_repo=no
    if [ -z $1 ] ; then
	echo 'if_repo_changed: You did not specify /path/to/local/repo. Stopping...' ; return 1;
    fi 
    if cd $1 && git status >& /dev/null ; then
	git fetch origin master
	#if [ ! -z "$(git status | grep '"git push"')" ] ; then git push ; fi #check if we haves commits which are not pushed yet
	if [ ! -z "$(git status | grep 'Your branch is ahead')" ] ; then updated_repo=yes ; git push ; fi
	git_status_output=$(git status -s) # we have something to commit
	if [ ${#git_status_output} -gt 0 ] ; then
	    echo "updating remote repo"
            if update_repo_remotely . ; then echo "repo $1 was updated remotely"; return 0; fi
	fi
	git_dif_output=$(git diff origin/master master)
	if [ ${#git_dif_output} -gt 0 ] ; then
	    echo "updating local repo"
	    if update_repo_locally . ; then  echo "repo $1 was updated locally" ; return 0 ; fi
	fi
	echo "Now repo $1 has nothing to update"	
	return 2
    else
	echo 'No repo found: ' $1
	return 1
    fi
}
export -f if_repo_changed
