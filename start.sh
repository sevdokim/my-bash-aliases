if [ -z $MY_BASH_ALIASES_STARTING_DIRECTORY ] ; then export MY_BASH_ALIASES_STARTING_DIRECTORY=$(pwd) ; fi
THIS_FILE_NAME=$(echo $BASH_SOURCE | awk -F/ '{print $NF}')
THIS_PATH=${BASH_SOURCE%$THIS_FILE_NAME}
if [ -z $THIS_PATH ] ; then THIS_PATH=$(pwd)'/' ; fi
if [ -z $(which git) ] ; then
    echo 'No git found. Just sourceing aliases.sh' ;
    source ${THIS_PATH}aliases.sh
fi
export THIS_COMPUTER_NAME=$(uname -n)
if [ -f ~/.this_computer_name ] ; then THIS_COMPUTER_NAME=$(cat ~/.this_computer_name) ; fi
echo "$THIS_COMPUTER_NAME:$(pwd)  --->  You sourced $THIS_PATH/$THIS_FILE_NAME script!!!"
REPO_MY_BASH_ALIASES=$(git config --global --get user.mybashaliasesdir)
if [ -z $REPO_MY_BASH_ALIASES ] ; then
    echo "Seems you have not install my-bash-aliases repository or ~/.gitconfig file have no information about it. Checking $THIS_PATH/ ..." ;
    if cd $THIS_PATH && git status >& /dev/null ; then
	echo "I found  $(cat repo-name) in $THIS_PATH"
	if [ $(cat repo-name) = my-bash-aliases ] ; then
	    REPO_MY_BASH_ALIASES=$PWD
	    echo "Running post-install.sh script!!!"
	    source post-install.sh
	    return 0
	else
	    echo 'It is not our repo. Stopping...'
	    return 1
	fi
    else
	echo 'No git repo in $THIS_PATH ... Starting new installation of my-bash-aliases'
	echo 'Using default installation path ~/.bash_aliases'
	if [ -e ~/.bash_aliases ] ; then
	    echo "I found ~/.bash_aliases. Moving it to ~/.bash_aliases.bkp"
	    mv ~/.bash_aliases ~/.bash_aliases.bkp
	fi
	git clone https://gitlab.cern.ch/sevdokim/my-bash-aliases ~/.bash_aliases
	echo "Running post-install.sh script!!!"
	source post-install.sh
	return 0
    fi
else
    if cd $REPO_MY_BASH_ALIASES && git status >& /dev/null ; then
	if [ $(cat repo-name) = my-bash-aliases ] ; then
	    echo "I found my-bash-alieses repository in $REPO_MY_BASH_ALIASES"
	    if [ ! -e config/config.$THIS_COMPUTER_NAME ] ; then
		echo "No config/config.$THIS_COMPUTER_NAME file found. Starting post-install.sh to create it."
		source post-install.sh ; return 0 ; fi 
	    for var in $(cat config/config.$THIS_COMPUTER_NAME)
	    do
		echo "I read $var from config/config.$THIS_COMPUTER_NAME"
		export $var
	    done
	    # check if we need to install some packages
	    if [ ! -z "$MY_BASH_ALIASES_INSTALLED_PACKAGES" ] ; then
		echo "I found list of system packeges to be be installed at $THIS_COMPUTER_NAME:"
		echo "$MY_BASH_ALIASES_INSTALLED_PACKAGES"
		for pack in $(echo "$MY_BASH_ALIASES_INSTALLED_PACKAGES" | awk -F: '{ for ( i=1; i<=NF; i++ ) print $i }') ; do
		    if [ -n "$(dpkg -l | grep ' '$pack' ')" ] ; then 
			echo "$pack is intalled"  
		    else 
			echo "You need to install $pack"  
		    fi  
		done
	    fi
	    # check other repos installed at this machines
	    if [ ! -z "$MY_BASH_ALIASES_INSTALLED_REPOS" ] ; then
                echo "I found list of repos to be be installed at $THIS_COMPUTER_NAME:"
                echo "$MY_BASH_ALIASES_INSTALLED_REPOS"
                for repo in $(echo "$MY_BASH_ALIASES_INSTALLED_REPOS" | awk -F: '{ for ( i=1; i<=NF; i++ ) print $i }') ; do
		    repo_string="$(grep $repo my-repos.list)"
                    if [ -n "$repo_string" ] ; then
                        echo "Found repo $repo in my-repos.list"
			REPO_DIR_NAME=$(echo $repo_string | awk '{print $3}')
			REPO_DIR="$(grep $REPO_DIR_NAME config/config.$THIS_COMPUTER_NAME)"
			if [ -n "$REPO_DIR" ] ; then 
			    echo "Configured to be installed in $REPO_DIR"
			    export $REPO_DIR
			    echo "Checking ${$REPO_DIR_NAME}" 
			else
			    echo "I did not find $REPO_DIR_NAME in config/config.$THIS_COMPUTER_NAME"
			    echo "Checking default location $HOME/$REPO_DIR_NAME"
			    export "${REPO_DIR_NAME}=$HOME/$REPO_DIR_NAME"
			fi
			#if cd $"$REPO_DIR_NAME"
                    else
                        echo "No repo $repo found in my-repos.list"
                    fi
                done
            fi
	    echo "Loading functions from repo.sh "
	    source $(pwd)/repo.sh
	    echo "Checking if repo changed"
	    if if_repo_changed $(pwd) ; then return 0; fi #no need to proceed because post-install.sh is called in case when repo was changed. It initiate start.sh again
	    echo "Sourcing aliases.sh finally."
	    source $(pwd)/aliases.sh	
	    echo "I finished loading of my-bash-aliases. Good by!"
	    if [ ! -z $MY_BASH_ALIASES_STARTING_DIRECTORY ] ; then 
		cd $MY_BASH_ALIASES_STARTING_DIRECTORY 
		unset MY_BASH_ALIASES_STARTING_DIRECTORY
	    fi
	    return 0
	else
	    echo "$REPO_MY_BASH_ALIASES is not our repo. Stopping..."
	    return 1
	fi
    else
	echo "I cannot find my-bash-aliases local repo in $REPO_MY_BASH_ALIASES "
	echo "Please check. Stopping..."
	return 1
    fi
fi


