function cd_and_set_term_title () {
    builtin cd "$@"
    case $PWD in
	'/')    TERM_TITLE='/' ;;
	$HOME)  TERM_TITLE='~' ;;
	*)      TERM_TITLE=$( pwd | awk -F/ '{ print $NF }') ;;
    esac
    TERM_TITLE=$(uname -n):$TERM_TITLE
    PROMPT_COMMAND='echo -en "\033]0;$TERM_TITLE\a"'
    return 0
}
export -f cd_and_set_term_title
alias cd='cd_and_set_term_title'
cd_and_set_term_title .
export PS1="\A@\h:\W> "
alias cp='/bin/cp -a'
if [ ! -z $(which emacs) ] ; then alias edit='emacs -nw' ; fi
alias ls='/bin/ls --color'
function k_check () {
    if [ ! -z $(which kinit) ] ; then
	if ! klist -s ; then
	    echo 'kerberos ticket is not valid, running "kinit sevdokim"'
	    kinit sevdokim
	fi
    else
	echo 'no kerberos configured'
    fi
}
export -f k_check
echo "Aliases from $BASH_SOURCE loaded! Note: PWD = $(pwd)"
alias clu_ssh='k_check ; ssh -Y sevdokim@uic7.m45.ihep.su'
