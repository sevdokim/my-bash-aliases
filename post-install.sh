cd $REPO_MY_BASH_ALIASES
git config --global --add user.mybashaliasesdir $REPO_MY_BASH_ALIASES
#git pull --rebase
if [ -e congig/config.$THIS_COMPUTER_NAME ] ;
then
    MESSAGE="Reconfigured on $THIS_COMPUTER_NAME" ;
else
    MESSAGE="New installation at $THIS_COMPUTER_NAME" ;
    touch config/config.$THIS_COMPUTER_NAME
fi
if [ -z "$(grep "REPO_MY_BASH_ALIASES=$REPO_MY_BASH_ALIASES" config/config.$THIS_COMPUTER_NAME)" ] ;
then 
    echo "REPO_MY_BASH_ALIASES="$REPO_MY_BASH_ALIASES >> config/config.$THIS_COMPUTER_NAME ; 
    git add config/config.$THIS_COMPUTER_NAME
    git commit -a -m "$MESSAGE"
    git status
    echo "Doing git push to add config/config.$THIS_COMPUTER_NAME"
    git push
fi
if [ -z "$(grep "source $REPO_MY_BASH_ALIASES/start.sh" ~/.bashrc)" ] ; then echo "source $REPO_MY_BASH_ALIASES/start.sh" >> ~/.bashrc ; fi
source $REPO_MY_BASH_ALIASES/start.sh
